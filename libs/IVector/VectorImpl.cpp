//
// Created by markp on 12.06.2020.
//

#include <cmath>
#include <new>
#include "../../include/IVector.h"

namespace {
    class VectorImpl : IVector {
        double * pData;
        int dim;
//        ILogger * logger;

    public:
        VectorImpl(int dim, double * pData);
        ~VectorImpl() override;
        IVector* clone() const override;
        double getCoord(size_t index) const override;
        RESULT_CODE setCoord(size_t index, double value) override;
        double norm(NORM norm) const override;
        size_t getDim() const override;
    };
}

VectorImpl::VectorImpl(int dim, double * pData) : dim(dim), pData(pData) {

}

size_t VectorImpl::getDim() const {
    return this->dim;
}

enum RESULT_CODE VectorImpl::setCoord(size_t index, double value) {
    if (index < 0 || index >= this->dim ) {
        return RESULT_CODE::OUT_OF_BOUNDS;
    } else if (std::isnan(value)) {
        return RESULT_CODE::NAN_VALUE;
    }
    this->pData[index] = value;
    return RESULT_CODE::SUCCESS;
}

double VectorImpl::getCoord(size_t index) const {
    if (index >= this->dim) {
        return NAN;
    }
    return this->pData[index];
}

IVector * VectorImpl::clone() const {
    size_t dim = getDim();
    double * data = new(std::nothrow) double[dim];
    if (!data) {
        return nullptr;
    }
    for (int i = 0; i < dim; ++i) {
        data[i] = getCoord(i);
    }
    VectorImpl * result = new(std::nothrow) VectorImpl(dim, data);
    return result;
}

VectorImpl::~VectorImpl() {
    delete [] pData;
}

double VectorImpl::norm(enum IVector::NORM norm) const {
    double n = 0;
    switch (norm) {
        case IVector::NORM::NORM_1:
            for (int i = 0; i < this->dim; ++i) {
                n += fabs(getCoord(i));
            }
            break;
        case IVector::NORM::NORM_2:
            for (int i = 0; i < this->dim; ++i) {
                n += getCoord(i) * getCoord(i);
            }
            break;
        case IVector::NORM::NORM_INF:
            n = fabs(getCoord(0));
            for (int i = 1; i < this->dim; ++i) {
                n = fmax(n, fabs(getCoord(i)));
            }
            break;
    }
    return n;
}



//
// Created by markp on 12.06.2020.
//
#define EXPORTING

#include <cmath>
#include <cstring>
#include <new>
#include "../../include/IVector.h"

#include "VectorImpl.cpp"




static void logIf(ILogger * pLogger, char const* pMsg, RESULT_CODE err) {
    if (pLogger) {
        pLogger->log(pMsg, err);
    }
}

IVector* IVector::add(IVector const *pOperand1, IVector const *pOperand2, ILogger *pLogger) {
    if (!pOperand1 || !pOperand2 || pOperand1->getDim() != pOperand2->getDim()) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::WRONG_ARGUMENT);
        return nullptr;
    }
    auto dim = pOperand1->getDim();
    auto * buf = new(std::nothrow)double[dim];
    if (!buf) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        return nullptr;
    }
    for (int i = 0; i < dim; ++i) {
        double sum = pOperand1->getCoord(i) + pOperand2->getCoord(i);
        if (std::isnan(sum)) {
            logIf(pLogger, __FUNCTION__, RESULT_CODE::CALCULATION_ERROR);
            delete [] buf;
            return nullptr;
        }
        buf[i] = sum;
    }

    VectorImpl * result = new(std::nothrow) VectorImpl(dim, buf);
    if (!result) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        delete [] buf;
        return nullptr;
    }
    return (IVector *)result;
}

IVector* IVector::sub(IVector const *pOperand1, IVector const *pOperand2, ILogger *pLogger) {
    if (!pOperand1 || !pOperand2 || pOperand1->getDim() != pOperand2->getDim()) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::WRONG_ARGUMENT);
        return nullptr;
    }
    auto dim = pOperand1->getDim();
    auto * buf = new(std::nothrow)double[dim];
    if (!buf) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        return nullptr;
    }
    for (int i = 0; i < dim; ++i) {
        double diff = pOperand1->getCoord(i) - pOperand2->getCoord(i);
        if (std::isnan(diff)) {
            logIf(pLogger, __FUNCTION__, RESULT_CODE::CALCULATION_ERROR);
            delete [] buf;
            return nullptr;
        }
        buf[i] = diff;
    }

    VectorImpl * result = new(std::nothrow) VectorImpl(dim, buf);
    if (!result) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        delete [] buf;
        return nullptr;
    }
    return (IVector *)result;
}

double IVector::mul(IVector const *pOperand1, IVector const *pOperand2, ILogger *pLogger) {
    if (!pOperand1 || !pOperand2 || pOperand1->getDim() != pOperand2->getDim()) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::WRONG_ARGUMENT);
        return NAN;
    }
    auto dim = pOperand1->getDim();
    double sum = 0;
    for (int i = 0; i < dim; ++i) {
        sum += pOperand1->getCoord(i) * pOperand2->getCoord(i);
        if (std::isnan(sum)) {
            logIf(pLogger, __FUNCTION__, RESULT_CODE::CALCULATION_ERROR);
            return NAN;
        }
    }
    return sum;
}

IVector* IVector::mul(IVector const* pOperand1, double scaleParam, ILogger* pLogger) {
    if (!pOperand1 || std::isnan(scaleParam)) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::WRONG_ARGUMENT);
        return nullptr;
    }
    auto dim = pOperand1->getDim();
    auto * buf = new(std::nothrow)double[dim];
    if (!buf) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        return nullptr;
    }
    for (int i = 0; i < dim; ++i) {
        double coord = pOperand1->getCoord(i) * scaleParam;
        if (std::isnan(coord)) {
            logIf(pLogger, __FUNCTION__, RESULT_CODE::CALCULATION_ERROR);
            delete [] buf;
            return nullptr;
        }
        buf[i] = coord;
    }

    VectorImpl * result = new(std::nothrow) VectorImpl(dim, buf);
    if (!result) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        delete [] buf;
        return nullptr;
    }
    return (IVector *)result;
}

RESULT_CODE IVector::equals(IVector const* pOperand1, IVector const* pOperand2, IVector::NORM norm, double tolerance, bool* result, ILogger* pLogger) {
    IVector * diff = IVector::sub(pOperand1, pOperand2, pLogger);
    if (diff == nullptr) {
        *result = false;
        return RESULT_CODE::WRONG_ARGUMENT;
    } else if (diff->norm(norm) <= tolerance) {
        delete diff;
        *result = true;
        return RESULT_CODE::SUCCESS;
    } else {
        delete diff;
        *result = false;
        return RESULT_CODE::SUCCESS;
    }
}

class IVector * IVector::createVector(size_t dim, double * pData, class ILogger * pLogger) {
    if (std::isnan(dim) || !pData) {
        if (pLogger)
            logIf(pLogger, __FUNCTION__, RESULT_CODE::WRONG_ARGUMENT);
        return nullptr;
    }
    for (int i = 0; i < dim; ++i) {
        if (std::isnan(pData[i])) {
            logIf(pLogger, __FUNCTION__, RESULT_CODE::NAN_VALUE);
            return nullptr;
        }
    }

    double * points = new(std::nothrow) double[dim];
    if (!points) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        return nullptr;
    }
    memcpy(points, pData, dim * sizeof(double));
    IVector * res = (IVector *)new(std::nothrow) VectorImpl(dim, points);
    if (!res) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        return nullptr;
    }
    return res;
}

IVector::~IVector() = default;
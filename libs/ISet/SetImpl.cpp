//
// Created by markp on 13.06.2020.
//

#include <ISet.h>
#include <vector>

namespace {
    class SetImpl : ISet {
        int dim;
        std::vector<const IVector *> data;
    public:
        SetImpl();
        ~SetImpl();
        RESULT_CODE insert(const IVector* pVector,IVector::NORM norm, double tolerance);
        RESULT_CODE get(IVector*& pVector, size_t index) const;
        RESULT_CODE get(IVector*& pVector, IVector const* pSample, IVector::NORM norm, double tolerance) const override;
        size_t getDim() const; //space dimension
        size_t getSize() const; //num elements in set
        void clear(); // delete all
        RESULT_CODE erase(size_t index);
        RESULT_CODE erase(IVector const* pSample, IVector::NORM norm, double tolerance);
        ISet* clone() const;
    };
}

size_t SetImpl::getDim() const {
    return dim;
}

size_t SetImpl::getSize() const {
    return data.size();
}

void SetImpl::clear() {
    int size = data.size();
    for (int i = 0; i < size; i++) {
        delete data[i];
    }
    data.clear();
}

RESULT_CODE SetImpl::erase(size_t index) {
    if (index < 0 || index >= data.size()) {
        return RESULT_CODE::NOT_FOUND;
    }
    delete data[index];
    data.erase(data.begin() + index);

    if (data.empty()) {
        data.clear();
        dim = 0;
    }

    return RESULT_CODE::SUCCESS;
}

RESULT_CODE SetImpl::erase(const IVector * pSample, IVector::NORM norm, double tolerance) {
    if (!pSample) {
        return RESULT_CODE::WRONG_ARGUMENT;
    }
    int size = data.size();
    for (size_t i = 0; i < size; ++i) {
        bool isEqual = false;
        IVector::equals(pSample, data[i], norm, tolerance, &isEqual, nullptr);
        if (isEqual) {
            return erase(i);
        }
    }
    return RESULT_CODE::NOT_FOUND;
}

ISet* SetImpl::clone() const {
    ISet * result = createSet(nullptr);
    if (!result) {
        return nullptr;
    }
    for (const IVector * element : data) {
        RESULT_CODE wasInsert = result->insert(element->clone(), IVector::NORM::NORM_2, 0);
        if (wasInsert != RESULT_CODE::SUCCESS) {
            result->clear();
            delete result;
            return nullptr;
        }
    }
    return result;
}

SetImpl::SetImpl() : dim(0) {}

SetImpl::~SetImpl() {
    int size = data.size();
    for (int i = 0; i < size; i++) {
        delete data[i];
    }
}

enum RESULT_CODE SetImpl::insert(const IVector * pVector, IVector::NORM norm, double tolerance) {
    if (!dim) {
        dim = pVector->getDim();
    } else if (dim != pVector->getDim()) {
        return RESULT_CODE::WRONG_DIM;
    }
    for (const IVector * element : data) {
        bool isEq = false;
        RESULT_CODE state = IVector::equals(element, pVector, norm, tolerance, &isEq, nullptr);
        if (state != RESULT_CODE::SUCCESS) {
            return RESULT_CODE::CALCULATION_ERROR;
        }
        if (isEq) {
            //такой элемент уже есть
            return RESULT_CODE::SUCCESS;
        }
    }
    data.push_back(pVector);
    return RESULT_CODE::SUCCESS;
}

RESULT_CODE SetImpl::get(IVector *&pVector, size_t index) const {
    if (data.size() <= index) {
        return  RESULT_CODE::OUT_OF_BOUNDS;
    }
    pVector = const_cast<IVector *>(data[index]);
    return RESULT_CODE::SUCCESS;
}

RESULT_CODE SetImpl::get(IVector *&pVector, IVector const * pSample, IVector::NORM norm, double tolerance) const {
    if (!pSample) {
        return RESULT_CODE::OUT_OF_MEMORY;
    }
    for (const IVector * element : data) {
        bool isEqual = false;
        IVector::equals(element, pSample, norm, tolerance, &isEqual, nullptr);
        if (isEqual) {
            pVector = const_cast<IVector *>(element);
            return RESULT_CODE::SUCCESS;
        }
    }
    return RESULT_CODE::NOT_FOUND;
}
//
// Created by markp on 13.06.2020.
//

#include "../../include/ISet.h"
#include "SetImpl.cpp"

static void logIf(ILogger * pLogger, char const* pMsg, RESULT_CODE err) {
    if (pLogger) {
        pLogger->log(pMsg, err);
    }
}

ISet* ISet::createSet(ILogger* pLogger) {
    ISet * result = (ISet *)new(std::nothrow) SetImpl();
    if (!result) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        return nullptr;
    }
    return result;
}

//RESULT_CODE isSame() {
//
//}

ISet* ISet::add(ISet const* pOperand1, ISet const* pOperand2, IVector::NORM norm, double tolerance, ILogger* pLogger) {
    if (pOperand1->getDim() != pOperand2->getDim()) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::WRONG_DIM);
        return nullptr;
    }
    ISet * result = pOperand1->clone();
    if (!result) {
        return nullptr;
    }
    size_t size = pOperand2->getSize();
    for (size_t i = 0; i < size; ++i) {
        IVector * element;
        RESULT_CODE isGet = pOperand2->get(element, i);
        if (isGet != RESULT_CODE::SUCCESS) {
            delete result;
            return nullptr;
        }
        RESULT_CODE isInsert = result->insert(element->clone(), norm, tolerance);
        if (isInsert != RESULT_CODE::SUCCESS) {
            delete result;
            return nullptr;
        }
    }
    return result;
}

ISet* ISet::intersect(ISet const* pOperand1, ISet const* pOperand2, IVector::NORM norm, double tolerance, ILogger* pLogger) {
    ISet * result = createSet(nullptr);
    if (!result)
        return nullptr;
    size_t size1 = pOperand1->getSize();
    size_t size2 = pOperand2->getSize();

    for (int i = 0; i < size1; ++i) {
        for (int j = 0; j < size2; ++j) {
            bool isEqual = false;
            IVector * element1, * element2;
            pOperand1->get(element1, i);
            pOperand1->get(element2, i);
            if (!element1 || !element2) {
                delete result;
                return nullptr;
            }
            IVector::equals(element1, element2, norm, tolerance, &isEqual, nullptr);
            if (isEqual) {
                RESULT_CODE canInsert = result->insert(element1->clone(), norm, tolerance);
                if (canInsert != RESULT_CODE::SUCCESS) {
                    delete result;
                    return nullptr;
                }
            }
        }
    }
    return result;
}

ISet* ISet::sub(ISet const* pOperand1, ISet const* pOperand2, IVector::NORM norm, double tolerance, ILogger* pLogger) {
    ISet * result = pOperand1->clone();
    if (!result) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        return nullptr;
    }
    size_t size2 = pOperand2->getSize();
    for (int i = 0; i < size2; ++i) {
        IVector * element;
        RESULT_CODE canGet = pOperand2->get(element, i);
        if (canGet != RESULT_CODE::SUCCESS) {
            logIf(pLogger, __FUNCTION__, canGet);
            delete result;
            return nullptr;
        }
        result->erase(element, norm, tolerance);
    }
    return result;
}

ISet* ISet::symSub(ISet const* pOperand1, ISet const* pOperand2, IVector::NORM norm, double tolerance, ILogger* pLogger) {

    ISet * u = add(pOperand1, pOperand2, norm, tolerance, pLogger);
    if (!u) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        return nullptr;
    }
    ISet * i = intersect(pOperand1, pOperand2, norm, tolerance, pLogger);
    if (!i) {
        logIf(pLogger, __FUNCTION__, RESULT_CODE::OUT_OF_MEMORY);
        delete u;
        return nullptr;
    }
    ISet * result = sub(u, i, norm, tolerance, pLogger);

    delete u;
    delete i;

    return result;
}

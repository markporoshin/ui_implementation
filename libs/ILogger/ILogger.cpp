//
// Created by markp on 12.06.2020.
//

#define EXPORTING

#include "../../include/ILogger.h"
#include "LoggerImpl.cpp"
#include <new>

ILogger* ILogger::createLogger(void *pClient) {
    return (ILogger *)new(std::nothrow)LoggerImpl();
}

void ILogger::log(const char * pMsg, enum RESULT_CODE err) {

}

ILogger::~ILogger() {

}
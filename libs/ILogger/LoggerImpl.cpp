//
// Created by markp on 13.06.2020.
//

#include <RC.h>

namespace {
    class LoggerImpl {
    public:
        void destroyLogger(void* pClient);
        void log(char const* pMsg, RESULT_CODE err);
        RESULT_CODE setLogFile(char const* pLogFile);

        LoggerImpl() = default;

    protected:
        ~LoggerImpl();
    };
}

void LoggerImpl::destroyLogger(void *pClient) {

}

void LoggerImpl::log(const char * pMsg, enum RESULT_CODE err) {

}

enum RESULT_CODE LoggerImpl::setLogFile(const char * pLogFile) {

}


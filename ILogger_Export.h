
#ifndef ILogger_EXPORT_H
#define ILogger_EXPORT_H

#ifdef ILogger_BUILT_AS_STATIC
#  define ILogger_EXPORT
#  define ILOGGER_NO_EXPORT
#else
#  ifndef ILogger_EXPORT
#    ifdef ILogger_EXPORTS
        /* We are building this library */
#      define ILogger_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define ILogger_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef ILOGGER_NO_EXPORT
#    define ILOGGER_NO_EXPORT 
#  endif
#endif

#ifndef ILOGGER_DEPRECATED
#  define ILOGGER_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef ILOGGER_DEPRECATED_EXPORT
#  define ILOGGER_DEPRECATED_EXPORT ILogger_EXPORT ILOGGER_DEPRECATED
#endif

#ifndef ILOGGER_DEPRECATED_NO_EXPORT
#  define ILOGGER_DEPRECATED_NO_EXPORT ILOGGER_NO_EXPORT ILOGGER_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef ILOGGER_NO_DEPRECATED
#    define ILOGGER_NO_DEPRECATED
#  endif
#endif

#endif /* ILogger_EXPORT_H */

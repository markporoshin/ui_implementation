//
// Created by markp on 13.06.2020.
//

#ifndef UI_LIB_EXPORT_H
#define UI_LIB_EXPORT_H


#ifdef EXPORTING
#define LIB_API    __declspec(dllexport)
#else
#define LIB_API    __declspec(dllimport)
#endif


#endif //UI_LIB_EXPORT_H

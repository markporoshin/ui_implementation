//
// Created by markp on 12.06.2020.
//

#ifndef UI_RC_H
#define UI_RC_H

enum class RESULT_CODE {
    SUCCESS,
    OUT_OF_MEMORY,
    BAD_REFERENCE,
    WRONG_DIM,
    DIVISION_BY_ZERO,
    NAN_VALUE,
    FILE_ERROR,
    OUT_OF_BOUNDS,
    NOT_FOUND,
    WRONG_ARGUMENT,
    CALCULATION_ERROR,
    MULTIPLE_DEFINITION
};

#endif //UI_RC_H

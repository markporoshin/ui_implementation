cmake_minimum_required(VERSION 3.15)
project(UI)

set(CMAKE_CXX_STANDARD 11)
include_directories(include)

add_subdirectory(libs)

add_executable(UI main.cpp ILogger_Export.h include/lib_export.h libs/ISet/ISet.cpp libs/ISet/SetImpl.cpp include/ISet.h include/doctest.h test/IVector/ivector_test.cpp)

target_link_libraries(UI ILogger)
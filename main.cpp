#include <iostream>

#include "ILogger.h"

int main() {
    std::cout << "Hello, World!" << std::endl;

    ILogger * logger = ILogger::createLogger(nullptr);

    RESULT_CODE  resultCode = logger->setLogFile(nullptr);


    return 0;
}
